import numpy as np
import pandas as pd
from pandas import DataFrame
import hashlib
import csv

df = pd.read_csv("./dados-brutos/SAMU_FEV_COMPLETO.csv", usecols = ['Nome','Idade', 'Sexo', 'RUA'])

with open('./dados-processados/SAMU_FEV_PROCESSADO.csv', mode='w') as employee_file:
    for index, row in df.iterrows():
        nome_md5 = hashlib.md5(str(row['Nome']).encode('utf-8')).hexdigest()
        row['Nome'] = int(nome_md5, 16)

        sexo_md5 = hashlib.md5(str(row['Sexo']).encode('utf-8')).hexdigest()
        row['Sexo'] = int(sexo_md5, 16)

        rua_md5 = hashlib.md5(row['RUA'].encode('utf-8')).hexdigest()
        row['RUA'] = int(rua_md5, 16)

        #print(row['Nome'], row['Sexo'])

        #escreve no csv
        doc_writer = csv.writer(employee_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        if np.isnan(row['Idade']):
            row['Idade'] = 0.0
        doc_writer.writerow([row['Nome'], row['Idade'], row['Sexo'],row['RUA'], 'Vítima'])

